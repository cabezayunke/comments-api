module.exports = {
    "entitySample": {
        "__v": 0,
        "parentItemId": "5984a997f3bf94001cb97bec",
        "body": "This is my first comment",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "createdBy": {
            "userId": "someUserId",
            "name": "someUserName"
        },
        "_id": "5984a997f3bf94001cb97bec"
    },
    "updatedEntitySample": {
        "__v": 0,
        "parentItemId": "5984a997f3bf94001cb97bec",
        "body": "This is my first UPDATED comment",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "createdBy": {
            "userId": "someUserId",
            "name": "someUserName"
        },
        "_id": "5984a997f3bf94001cb97bec"
    },
    "invalidIds": ["invalidId", 3245345, null],
    "invalidPostData": [
        {
            "body": "missing parentItemId",
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        },
        {
            "parentItemId": "missing body",
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        },
        {
            "parentItemId": "5984a997f3bf94001cb97bec",
            "body": "missing createdBy"
        },
        {
            "parentItemId": 235645267,
            "body": "invalid parentItemId",
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        },
        {
            "parentItemId": "",
            "body": "empty parentItemId",
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        },
        {
            "parentItemId": "invalid body",
            "body": 56256,
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        },
        {
            "parentItemId": "empty body",
            "body": "",
            "createdBy": {
                "userId": "someUserId",
                "name": "someUserName"
            }
        }
    ],
    "invalidPutData": [
        {
            "parentItemId": 235645267,
            "body": "invalid parentItemId"
        },
        {
            "parentItemId": "",
            "body": "empty parentItemId"
        },
        {
            "parentItemId": "invalid body",
            "body": 56256
        },
        {
            "parentItemId": "empty body",
            "body": ""
        }
    ]
};