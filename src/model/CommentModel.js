import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import Promise from 'bluebird';

// make mongoose use bluebird
mongoose.Promise = Promise;

/**
 * Mongoose schema
 */
const schema = mongoose.Schema(
    {
        _id:  {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
            auto: true
        },
        parentItemId: {
            type: String,
            required: true
        },
        createdBy: {
            userId: String,
            name: String,
            photoUrl: String
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        body: {
            type: String,
            required: true
        }
    }
);

schema.plugin(mongoosePaginate);

// create model from schema
const CommentModel = mongoose.model('CommentModel', schema);
export default CommentModel;