import { Router } from 'express';
import CommentsController from './controller/CommentsController';

const router = new Router();

/*
 * Comments API routes
 */
const controller = new CommentsController();
router.get('/comments/:id', controller.getCommentById);
router.get('/comments', controller.getComments);
router.post('/comments', controller.crateNewComment);
router.delete('/comments/:id', controller.deleteComment);
router.put('/comments/:id', controller.updateComment);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

export default router;