'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _CommentsController = require('./controller/CommentsController');

var _CommentsController2 = _interopRequireDefault(_CommentsController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();

/*
 * Comments API routes
 */
var controller = new _CommentsController2.default();
router.get('/comments/:id', controller.getCommentById);
router.get('/comments', controller.getComments);
router.post('/comments', controller.crateNewComment);
router.delete('/comments/:id', controller.deleteComment);
router.put('/comments/:id', controller.updateComment);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

exports.default = router;